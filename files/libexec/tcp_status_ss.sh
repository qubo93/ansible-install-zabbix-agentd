#!/bin/bash 
#scripts for tcp status 
function SYNRECV {
r_SYNRECV=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'SYN-RECV' | awk '{print $2}'`
if [[ ${r_SYNRECV} == "" ]];then
        echo "0"
else
        echo $r_SYNRECV
fi
}
function ESTAB {
r_ESTAB=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'ESTAB' | awk '{print $2}'`
if [[ ${r_ESTAB} == "" ]];then
        echo "0"
else
        echo $r_ESTAB
fi
}
function FINWAIT1 {
r_FINWAIT1=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'FIN-WAIT-1' | awk '{print $2}'`
if [[ ${r_FINWAIT1} == "" ]];then
        echo "0"
else
        echo $r_FINWAIT1
fi
}
function FINWAIT2 {
r_FINWAIT2=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'FIN-WAIT-2' | awk '{print $2}'`
if [[ ${r_FINWAIT2} == "" ]];then
        echo "0"
else
        echo $r_FINWAIT2
fi
}
function TIMEWAIT {
r_TIMEWAIT=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'TIME-WAIT' | awk '{print $2}'`
if [[ ${r_TIMEWAIT} == "" ]];then
        echo "0"
else
        echo $r_TIMEWAIT
fi
}
function LASTACK {
r_LASTACK=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'LAST-ACK' | awk '{print $2}'`
if [[ ${r_LASTACK} == "" ]];then
        echo "0"
else
        echo $r_LASTACK
fi
}
function LISTEN {
r_LISTEN=`/usr/sbin/ss -ant | awk '{++s[$1]} END {for(k in s) print k,s[k]}' | grep 'LISTEN' | awk '{print $2}'`
if [[ ${r_LISTEN} == "" ]];then
        echo "0"
else
        echo $r_LISTEN
fi
}
$1
